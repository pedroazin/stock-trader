package com.azin.stockTrader.services;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.azin.stockTrader.dtos.ActionStockDto;
import com.azin.stockTrader.enums.ActionEnum;
import com.azin.stockTrader.models.Inventory;
import com.azin.stockTrader.models.Stock;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ActionStockAvaliatorServiceTest {
	
	@Autowired
	private ActionStockAvaliatorService actionStockAvaliator;

	@Test
	public void mustBuy1000Quantities() {

		Stock stockWS = new Stock("ABC", 10.0);

		Inventory inventoryStock = new Inventory(new Stock("ABC", 8.0), 2000l);

		ActionStockDto actionStockDto = actionStockAvaliator.avaliate(inventoryStock, stockWS).get();

		ActionStockDto expect = new ActionStockDto("ABC", ActionEnum.BUY, 1000l);

		assertEquals(expect, actionStockDto);
	}

	@Test
	public void mustSellAllStock() {

		Stock stockWS = new Stock("ABC", 6.0);

		Inventory inventoryStock = new Inventory(new Stock("ABC", 8.0), 2000l);

		ActionStockDto actionStockDto = actionStockAvaliator.avaliate(inventoryStock, stockWS).get();

		ActionStockDto expect = new ActionStockDto("ABC", ActionEnum.SELL, 2000l);

		assertEquals(expect, actionStockDto);
	}

}
