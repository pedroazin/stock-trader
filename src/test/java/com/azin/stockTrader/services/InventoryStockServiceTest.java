package com.azin.stockTrader.services;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.azin.stockTrader.dtos.ActionStockDto;
import com.azin.stockTrader.enums.ActionEnum;
import com.azin.stockTrader.models.Inventory;
import com.azin.stockTrader.models.Stock;

@RunWith(SpringRunner.class)
@SpringBootTest
public class InventoryStockServiceTest {

	@Autowired
	private InventoryStockService inventoryStockService;

	@Test
	public void mustCreateListActionStock() {

		List<Stock> stocks = Arrays.asList(new Stock("ABC", 10.0), new Stock("BCA", 20.0), new Stock("CAB", 40.0));

		List<Inventory> inventaries = Arrays.asList(new Inventory(new Stock("ABC", 8.0), 1000l),
				new Inventory(new Stock("BCA", 25.0), 500l), new Inventory(new Stock("CAB", 38.0), 2000l));

		List<ActionStockDto> result = inventoryStockService.createActionStock(inventaries, stocks);

		ActionStockDto first = new ActionStockDto("ABC", ActionEnum.BUY, 1000l);
		ActionStockDto second = new ActionStockDto("BCA", ActionEnum.SELL, 500l);

		assertEquals(2, result.size());
		assertEquals(first, result.get(0));
		assertEquals(second, result.get(1));
	}
	
	

	@Test
	public void teste() {


		List<Inventory> result = inventoryStockService.listar();
		
		assertEquals(3, result.size());
	}
	
	

}
