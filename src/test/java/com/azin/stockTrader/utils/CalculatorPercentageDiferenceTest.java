package com.azin.stockTrader.utils;

import java.math.BigDecimal;

import org.junit.Test;

import com.azin.stockTrader.utils.CalculatorPercentageDiference;

import static org.junit.Assert.assertEquals;

public class CalculatorPercentageDiferenceTest {

	@Test
	public void calculateDiferenceWhenValueDecrease() {

		BigDecimal v1 = BigDecimal.valueOf(1.0);
		BigDecimal v2 = BigDecimal.valueOf(0.0);

		Integer calculate = CalculatorPercentageDiference.calculate(v1, v2);

		assertEquals(Integer.valueOf(-100), calculate);

	}

	@Test
	public void calculateDiferenceWhenValueIncrease() {

		BigDecimal v1 = BigDecimal.valueOf(1200.0);
		BigDecimal v2 = BigDecimal.valueOf(1320.0);

		Integer calculate = CalculatorPercentageDiference.calculate(v1, v2);

		assertEquals(Integer.valueOf(10), calculate);

	}

	@Test(expected = IllegalArgumentException.class)
	public void dontAcceptZeroValueInFirstParameter() {

		BigDecimal v1 = BigDecimal.valueOf(0);
		BigDecimal v2 = BigDecimal.valueOf(1320.0);

		CalculatorPercentageDiference.calculate(v1, v2);

	}

}
