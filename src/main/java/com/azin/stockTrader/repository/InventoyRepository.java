package com.azin.stockTrader.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.azin.stockTrader.models.Inventory;

public interface InventoyRepository extends JpaRepository<Inventory, Long> {

}
