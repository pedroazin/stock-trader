package com.azin.stockTrader.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.stereotype.Service;

public class CalculatorPercentageDiference {

	public static Integer calculate(BigDecimal v1, BigDecimal v2) {
		
		if(v1 == null || v1.equals(BigDecimal.ZERO))
			throw new IllegalArgumentException("The first parameter don't be null or zero");
		
		BigDecimal multiply = v2.multiply(BigDecimal.valueOf(100));
		
		return multiply.divide(v1,RoundingMode.CEILING).intValue() - 100;
	}

}
