package com.azin.stockTrader.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.azin.stockTrader.dtos.ActionStockDto;
import com.azin.stockTrader.models.Inventory;
import com.azin.stockTrader.models.Stock;
import com.azin.stockTrader.repository.InventoyRepository;

@Service
public class InventoryStockService {

	private ActionStockAvaliatorService actionStockAvaliatorService;
	private InventoyRepository repository;

	@Autowired
	public InventoryStockService(ActionStockAvaliatorService actionStockAvaliatorService, InventoyRepository repository) {
		this.actionStockAvaliatorService = actionStockAvaliatorService;
		this.repository = repository;
	}
	
	public List<Inventory> listar(){
		return repository.findAll();
	}

	public List<ActionStockDto> createActionStock(List<Inventory> inventoryStocks, List<Stock> stocksWs) {

		List<ActionStockDto> retorno = new ArrayList<>();

		Map<String, Stock> mapStocksWs = stocksWs.stream().collect(Collectors.toMap(Stock::getId, Function.identity()));

		inventoryStocks.forEach(inventoryStock -> {
			Stock stockWS = mapStocksWs.get(inventoryStock.getStock().getId());
			
			Optional<ActionStockDto> optional = actionStockAvaliatorService.avaliate(inventoryStock,
					stockWS);

			if (optional.isPresent()) {
				retorno.add(optional.get());
			}

		});

		return retorno;

	}

}
