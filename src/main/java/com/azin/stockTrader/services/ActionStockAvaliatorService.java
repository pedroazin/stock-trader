package com.azin.stockTrader.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.azin.stockTrader.dtos.ActionStockDto;
import com.azin.stockTrader.enums.ActionEnum;
import com.azin.stockTrader.models.Inventory;
import com.azin.stockTrader.models.Stock;
import com.azin.stockTrader.utils.CalculatorPercentageDiference;

@Service
public class ActionStockAvaliatorService {

	private static final int LOWER_PERCENT = -10;
	private static final int HIGHER_PERCENT = 10;

	@Autowired
	public ActionStockAvaliatorService() {
	}

	public Optional<ActionStockDto> avaliate(Inventory inventoryStock, Stock stockWS) {

		Stock stock = inventoryStock.getStock();

		Integer result = CalculatorPercentageDiference.calculate(stock.getPrice(), stockWS.getPrice());
		if (result >= HIGHER_PERCENT) {
			return Optional.of(new ActionStockDto(stockWS.getId(), ActionEnum.BUY, 1000l));
		} else if (result <= LOWER_PERCENT) {
			return Optional.of(new ActionStockDto(stockWS.getId(), ActionEnum.SELL, inventoryStock.getQuantity()));
		}
		return Optional.ofNullable(null);

	}

}
