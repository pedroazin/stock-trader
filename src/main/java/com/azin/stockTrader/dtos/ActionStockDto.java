package com.azin.stockTrader.dtos;

import java.io.Serializable;

import com.azin.stockTrader.enums.ActionEnum;

public class ActionStockDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String stock;

	private ActionEnum actionEnum;

	private Long amount;

	public ActionStockDto(String stock, ActionEnum actionEnum, Long amount) {
		super();
		this.stock = stock;
		this.actionEnum = actionEnum;
		this.amount = amount;
	}

	public String getStock() {
		return stock;
	}

	public ActionEnum getActionEnum() {
		return actionEnum;
	}

	public Long getAmount() {
		return amount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((actionEnum == null) ? 0 : actionEnum.hashCode());
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((stock == null) ? 0 : stock.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ActionStockDto other = (ActionStockDto) obj;
		if (actionEnum != other.actionEnum)
			return false;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (stock == null) {
			if (other.stock != null)
				return false;
		} else if (!stock.equals(other.stock))
			return false;
		return true;
	}

}
